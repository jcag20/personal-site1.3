import React from 'react';
// import logo from './logo.svg';
import './App.css';
import NavBar from './components/navBar';
import Home from './components/home';
import About from './about';
import foto from './components/images/about.jpg';
import { faHtml5, faCss3Alt } from '@fortawesome/free-brands-svg-icons';
import Skill from './components/skill';
import { faUser } from '@fortawesome/free-solid-svg-icons';

function App() {
  return (
    <div>
        <NavBar />    {/* barra de navedacion */}

        <Home />

        <div className='about'>
            <a href="#" id="about"></a>
            <h1 class="aguamark">About</h1>
            <div className='about-box'> 
                <div className='about-text'>
                        <p>Actualmente estoy desarrollando mis habilidades en el ámbito de la programación web. 
                            He decidido dedicarme a la programación ya que los distintos lenguajes te permiten poder llevar 
                            a la web, cualquier creación de tu imaginación. <br /><br />
                            Actualmente estudio Ingeniería en Sistemas en la Universidad de San Carlos, y estoy complementando
                            mis conocimientos en EduTec, en el curso de Front-End. 
                            <br /><br />
                            Me gustan los retos y estoy dispuesto a trabajar en proyectos que lo demanden. 
                            <br /><br />
                            Acá podrás conocer un poco más de mí. 
        
                        </p>                    
                </div>
                <div className='about-photo'>
                    <img src={foto} alt="about foto"/>
                </div>

                  <div className='about-skills'>
                    
                    <div className='skill-area'>
                        
                        <Skill icon={faHtml5} />
                        <Skill className='mid' icon={faCss3Alt} />
                        <Skill icon={faUser} />

                    </div>
                </div>
            </div>
        </div>

    </div>
  );
}

export default App;
