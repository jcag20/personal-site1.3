import React from 'react';
import ReactDOM from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


let clas = 'progress-bar ';
function Skill (props){
    return(
        
        <div className="skill">
            <p><i><FontAwesomeIcon icon={props.icon} /></i></p>
            <div className="progress">
                {props.habi ? clas.concat(props.habi) : clas }
                <div className={clas} ><span>60%</span></div>
            </div>
        </div>
    );
}

export default Skill;