import React from 'react';
import NavBarItem from '../components/navbar-item';
import { faUserCircle, faBaseballBall, faGlobe, faHome } from '@fortawesome/free-solid-svg-icons';

function NavBar(){
    return (
        //navbar con items, argumentos nombre del icono, href y texto 
        <div className="navbar">  
           <NavBarItem pin='#about' icon={faUserCircle} text='Acerca de mi'/>
           <NavBarItem icon={faBaseballBall} text='Pasatiempos'/>
           <NavBarItem icon={faGlobe} text='Redes Sociales'/>
           <NavBarItem pin='#home'icon={faHome} text='Home'/>



        </div>
    );
}


export default NavBar;