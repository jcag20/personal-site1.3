import React from 'react';
import ReactDOM from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function NavBarItem(props){
    return(
        <nav className='navbar-item'>
            <a href={props.pin} class="navbar-link">
                <i><FontAwesomeIcon icon={props.icon} /></i>
                <span>{props.text}</span>
            </a>
        </nav>
    );
}

export default NavBarItem;